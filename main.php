<?php
	require_once('../config.php');
	require_once('../model/Aluno.php');
	require_once('../model/Professor.php');
	require_once('../model/Turma.php');

	if($_SERVER['REQUEST_METHOD']=='POST' ) {

		$codigoTurma = $_POST['codTurma'];
		$_SESSION['alunos'] = buscaAlunos($codigoTurma);
		$_SESSION['professores'] = buscaProfessores($codigoTurma);

		var_dump($_SESSION['alunos']);
		var_dump($_SESSION['professores']);
		header('Location: ../index.php');
		
	}else{
		$_SESSION["error"] = 1;
	}

	function buscaAlunos($codigo){
		$obj = new Aluno();
		$obj->setTurma($codigo);
		$alunos = $obj->findAlunos($codigo);
		return $alunos;
	}

	function buscaProfessores($codigo){
		$obj = new Professor();
		$obj->setTurma($codigo);
		$professores = $obj->findProfessores();
		return $professores;	
	}

?>