<?php 

require_once(DBAPI);

class Aluno{

	private $ra;
	private $nome;
	private $turma;

	function __construct($ra = null,$nome = null, $turma = null){
		$this->ra = $ra;
		$this->nome = $nome;
		$this->turma = $turma;
	}
	
	public function getRa(){
		return $this->ra;
	}


	public function getNome(){
		return $this->nome;
	}


	public function getTurma(){
		return $this->turma;
	}

	public function setRa($ra){
		$this->ra = $ra;
	}

	public function setNome($nome){
		$this->nome = $nome;
	}

	public function setTurma($turma){
		$this->turma = $turma;
	}

	public function findAlunos(){
		$conn = new Database();
		if($conn){
			$stmt = $conn->prepare("SELECT * FROM aluno WHERE turma = ?");
			$stmt->execute(array($this->turma));
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
		}
		return $results;
	}

}


?>

