<?php
	session_start(); 
	require_once('config.php'); 
	require_once('controller/buscaTurmas.php');
	$turmas = buscaTurmas();
?>

<?php require_once(HEADER_TEMPLATE);?>

<body>
	<h1>Consulta de alunos</h1>
	<form action="controller/main.php" method="POST">
		<label>Turma:</label><br>
		<select name="codTurma">
			<?php
				foreach($turmas as $indice => $turma):
			?>
				<option value="<?php echo $turma["codigo"] ?>" ><?php echo $turma["descricao"] ." - ". $turma["periodo"]?></option>
					    
			<?php
				endforeach;
			?>
		</select><br><br>
		<input type="submit" name="btnFiltrar" value="Filtrar Alunos e Professores">
	</form>
<hr>
	<h4>Alunos:</h4>
	<table name='tb_alunos'>
		<thead>
			<tr>
				<th>RA</th>
				<th>Nome</th>
				<th>Turma</th>
			</tr>
		</thead>
		<tbody>
			<tr>
			<?php
				var_dump($_SESSION);
				foreach($_SESSION['alunos'] as $indice => $aluno):
			?>
				<td><?php echo $aluno["nome"] ." - ". $aluno["idade"]?></td>
					    
			<?php
				endforeach;
			?>	
			</tr>
		</tbody>
	</table>

	<h4>Professores</h4>
	<table name='tb_professores'>
		<thead>
			<tr>
				<th>Codigo</th>
				<th>Nome</th>
				<th>Turma</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				
			</tr>
		</tbody>
	</table>

</body>


<?php require_once(FOOTER_TEMPLATE);?>
