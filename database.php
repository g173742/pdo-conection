<?php 
	
	class Database{

		protected $conn = null;

		function __construct(){

			$dsn = 'mysql:host='.DB_HOST. ';dbname='.DB_NAME.';';
			$user = DB_USER;
			$password = DB_PASSWORD;
			try {
			    $this->conn = new PDO($dsn, $user, $password);
				$this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				$this->conn->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
				return $this->conn;
			} catch (PDOException $e) {
				echo $e->getMessage();
			}
		}

		function __destruct(){
			$this->conn = null;
		}

        public function prepare($sql){
       		return $this->conn->prepare($sql);
        }
    }


?>